import serial
import json

is_connected = False
REG_CONFIG_PATH = 'assets/reg_config/h2gcroc_1v4_r1.json'

ser = serial.Serial()

def load_reg_config():
    with open(REG_CONFIG_PATH) as reg_config_file:
        return json.load(reg_config_file)
    
def create_serial_connection(port, baudrate):
    global ser
    # check if port is available
    if port is None:
        raise Exception("Port is not specified")
    # check if baudrate is available
    if baudrate is None:
        raise Exception("Baudrate is not specified")
    # check if port exists
    # create serial connection
    ser = serial.Serial(port, baudrate, timeout=1)
    if ser.isOpen():
        print(ser.name + ' is open...')
    else:
        raise Exception("Failed to open serial port")

def send_one_group_to_h2gcroc(port, baudrate, h2gcrocid, keyname, valuelist):
    # print("Sending one group to H2GCROC")
    global is_connected
    if is_connected == False:
        try:
            create_serial_connection(port, baudrate)
            is_connected = True
        except Exception as e:
            print(e)
            is_connected = False
            return
    
    print("keyname: " + keyname)
    reg_config_data = load_reg_config()
    try:
        i2c_address = reg_config_data["I2C_address"][keyname]
    except Exception as e:
        print(e)
        return
    # print("i2c_address: " + str(i2c_address))

    list_to_send = []
    # print("valuelist:")
    for value in valuelist:
        # print in hex
        # print(str(hex(value)))
        # append to list
        list_to_send.append(value)
    cmd_len = len(list_to_send)

    list_to_send.insert(0, (i2c_address & 0x07) << 5)
    list_to_send.insert(0, (i2c_address >> 3))
    list_to_send.insert(0, cmd_len)

    list_to_send.insert(0, 0x00)
    list_to_send.insert(0, 0x00)
    list_to_send.insert(0, 0x00)

    list_to_send.insert(0, 0x03)
    list_to_send.insert(0, int(h2gcrocid, 16))

    while len(list_to_send) < 40:
        list_to_send.append(0x00)

    # print("len(list_to_send): " + str(len(list_to_send)))
    # print("list_to_send:")
    # for value in list_to_send:
    #     print(str(hex(value)))

    try:
        ser.write(serial.to_bytes(list_to_send))
    except Exception as e:
        print(e)
        is_connected = False
        return
    