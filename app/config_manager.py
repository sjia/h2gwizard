import dearpygui.dearpygui as dpg
import json
import yaml
import serial_comm
import udp_comm

REG_CONFIG_PATH = 'assets/reg_config/h2gcroc_1v4_r1.json'
FILE_EXTENSION = ".yaml"

dict_json_group_name = {
    "TopRegistersWindow": 'registers_top',
    "ChannelWindow": 'registers_channel_wise',
    "GlobalAnalogWindow": 'registers_global_analog',
    "ReferenceVoltageWindow": 'registers_reference_voltage',
    "MasterTDCWindow": 'registers_master_tdc',
    "DigitalHalfWindow": 'registers_digital_half',
}

RIGHT_SIDE_WINDOW_WIDTH = 310
LEFT_SIDE_WINDOW_WIDTH  = 290

all_window_tags = ["TopRegistersWindow", "ChannelWindow", "GlobalAnalogWindow", "ReferenceVoltageWindow", "MasterTDCWindow", "DigitalHalfWindow"]

SerialPort      = "/dev/cu.usbmodem101"
BaudRate        = "115200"
UDP_IP_TARGET   = "127.0.0.1"
UDP_PORT_TARGET = 5005
UDP_IP_SOURCE   = "127.0.0.1"
UDP_PORT_SOURCE = 5006
H2GCROC_ID      = "0xA5"

Mode = "UDP"
# Mode = "Serial"

comm_config = "assets/comm_config.json"
try:
    with open(comm_config) as comm_config_file:
        comm_config_data = json.load(comm_config_file)
        SerialPort = comm_config_data['SerialPort']
        BaudRate = comm_config_data['BaudRate']
        UDP_IP_TARGET = comm_config_data['UDP_IP_TARGET']
        UDP_PORT_TARGET = comm_config_data['UDP_PORT_TARGET']
        UDP_IP_SOURCE = comm_config_data['UDP_IP_SOURCE']
        UDP_PORT_SOURCE = comm_config_data['UDP_PORT_SOURCE']
        H2GCROC_ID = comm_config_data['H2GCROC_ID']
except Exception as e:
    print(e)

def on_close_callback(sender, app_data):
    global comm_config
    comm_config_data = {
        "SerialPort": SerialPort,
        "BaudRate": BaudRate,
        "UDP_IP_TARGET": UDP_IP_TARGET,
        "UDP_PORT_TARGET": UDP_PORT_TARGET,
        "UDP_IP_SOURCE": UDP_IP_SOURCE,
        "UDP_PORT_SOURCE": UDP_PORT_SOURCE,
        "H2GCROC_ID": H2GCROC_ID
    }
    with open(comm_config, 'w') as comm_config_file:
        json.dump(comm_config_data, comm_config_file)

def on_serial_port_changed(sender, app_data, user_data):
    global SerialPort
    SerialPort = app_data

def on_baud_rate_changed(sender, app_data, user_data):
    global BaudRate
    BaudRate = app_data
    # dpg.set_value("input_baud_rate", BaudRate)

def on_h2gcroc_id_changed(sender, app_data, user_data):
    global H2GCROC_ID
    H2GCROC_ID = app_data
    # dpg.set_value("input_h2gcroc_id", H2GCROC_ID)

def on_udp_ip_target_changed(sender, app_data, user_data):
    global UDP_IP_TARGET
    UDP_IP_TARGET = app_data

def on_udp_port_target_changed(sender, app_data, user_data):
    global UDP_PORT_TARGET
    UDP_PORT_TARGET = app_data

def on_udp_ip_source_changed(sender, app_data, user_data):
    global UDP_IP_SOURCE
    UDP_IP_SOURCE = app_data

def on_udp_port_source_changed(sender, app_data, user_data):
    global UDP_PORT_SOURCE
    UDP_PORT_SOURCE = app_data

def on_serial_send_one_clicked(sender, app_data, user_data):
    is_active_window_found = False
    for window_name in all_window_tags:
        if dpg.does_item_exist(window_name):
            is_active_window_found = True
            # print("window name: ", window_name)
            window_title = dpg.get_item_label(window_name)
            # print("window title: ", window_title)
            reg_key = window_title.split(': ')[1]
            # print("reg key: ", reg_key)
            for key, values in register_map.items():
                if key == reg_key:
                    # print("register map key: ", key)
                    print("register map values: ", values)
                    serial_comm.send_one_group_to_h2gcroc(SerialPort, BaudRate, H2GCROC_ID, key, values)
    if not is_active_window_found:
        print("No active window found!")

def on_serial_send_all_clicked(sender, app_data, user_data):
    for key, values in register_map.items():
        # print("register map key: ", key)
        print("register map values: ", values)
        serial_comm.send_one_group_to_h2gcroc(SerialPort, BaudRate, H2GCROC_ID, key, values)

def on_udp_send_one_clicked(sender, app_data, user_data):
    is_active_window_found = False
    for window_name in all_window_tags:
        if dpg.does_item_exist(window_name):
            is_active_window_found = True
            # print("window name: ", window_name)
            window_title = dpg.get_item_label(window_name)
            # print("window title: ", window_title)
            reg_key = window_title.split(': ')[1]
            # print("reg key: ", reg_key)
            for key, values in register_map.items():
                if key == reg_key:
                    # print("register map key: ", key)
                    print("register map values: ", values)
                    udp_comm.send_one_group_to_h2gcroc(UDP_IP_TARGET, UDP_PORT_TARGET, UDP_IP_SOURCE, UDP_PORT_SOURCE, H2GCROC_ID, key, values)
    if not is_active_window_found:
        print("No active window found!")

def on_udp_send_all_clicked(sender, app_data, user_data):
    for key, values in register_map.items():
        # print("register map key: ", key)
        print("register map values: ", values)
        udp_comm.send_one_group_to_h2gcroc(UDP_IP_TARGET, UDP_PORT_TARGET, UDP_IP_SOURCE, UDP_PORT_SOURCE, H2GCROC_ID, key, values)

def load_reg_config():
    with open(REG_CONFIG_PATH) as reg_config_file:
        return json.load(reg_config_file)
    
def brief_table(register_data, register_val, register_index, reg_group):
     with dpg.table(header_row=True, resizable=True, policy=dpg.mvTable_SizingStretchProp,
                   borders_outerH=True, borders_innerV=True, borders_innerH=True, borders_outerV=True):
        dpg.add_table_column(label="Bit 0", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 1", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 2", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 3", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 4", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 5", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 6", init_width_or_weight=0.1)
        dpg.add_table_column(label="Bit 7", init_width_or_weight=0.1)

        with dpg.table_row():
            for bit in register_data:
                _name_text = dpg.add_text(bit['name'])
                with dpg.tooltip(parent=_name_text):
                    dpg.add_text(bit['name'])
                    # if bit['description'] == "":
                    #     dpg.add_text(bit['name'])
                    # dpg.add_text(bit['description'])
                
        with dpg.table_row():
            for i in range(8):
                # make buttons for each bit, clicking them toggles the bit
                _bit = (register_val & (1 << i)) >> i
                _tag = "register_" + str(register_index) + "_bit_" + str(i)
                # button_info_str = f"Register #{register_index}, Bit #{i}, Value: {_bit}, Group: {reg_group}"
                # print(button_info_str)
                _button = dpg.add_button(label=str(_bit), height=20, width=75, tag=_tag, callback=on_bit_clicked, user_data=[_bit, register_index, i, reg_group])

def create_or_update_window(reg_config_data, user_data, group_name, window_tag):
    print("creating window: ", window_tag)
    print("user data: ", user_data)
    print("group name: ", group_name)
    global all_window_tags
    for window_name in all_window_tags:
        if dpg.does_item_exist(window_name):
            print("deleting window: ", window_name)
            dpg.delete_item(window_name)

    with dpg.window(tag=window_tag, width=700, height=850, no_resize=True):
        dpg.set_item_label(window_tag, window_tag + ': ' + group_name)
        dpg.set_item_pos(window_tag, pos=[290, 0])

        with dpg.group(horizontal=False):
            print("reg number: ", len(reg_config_data[dict_json_group_name[window_tag]][0].keys()))
            for i, reg_name in enumerate(reg_config_data[dict_json_group_name[window_tag]][0].keys()):
                with dpg.group():
                    dpg.add_text(f"Register #{i}", bullet=True)
                
                with dpg.group():
                    brief_table(reg_config_data[dict_json_group_name[window_tag]][0][reg_name], user_data[i], i, group_name)

def on_bit_clicked(sender, app_data, user_data):
    button_value, reg_index, bit_index, group_name = user_data

    button_value = 1 - button_value
    dpg.configure_item(sender, label=str(button_value), user_data=[button_value, reg_index, bit_index, group_name])

    update_register_map(group_name, reg_index, bit_index, button_value)

def on_top_sub_module_clicked(sender, app_data, user_data):
    reg_config_data = load_reg_config()
    create_or_update_window(reg_config_data, user_data, sender, "TopRegistersWindow")

def on_channel_module_clicked(sender, app_data, user_data):
    reg_config_data = load_reg_config()
    create_or_update_window(reg_config_data, user_data, sender, "ChannelWindow")

def on_global_analog_module_clicked(sender, app_data, user_data):
    reg_config_data = load_reg_config()
    create_or_update_window(reg_config_data, user_data, sender, "GlobalAnalogWindow")

def on_reference_voltage_module_clicked(sender, app_data, user_data):
    reg_config_data = load_reg_config()
    create_or_update_window(reg_config_data, user_data, sender, "ReferenceVoltageWindow")

def on_digital_half_module_clicked(sender, app_data, user_data):
    reg_config_data = load_reg_config()
    create_or_update_window(reg_config_data, user_data, sender, "DigitalHalfWindow")

def on_master_tdc_module_clicked(sender, app_data, user_data):
    reg_config_data = load_reg_config()
    create_or_update_window(reg_config_data, user_data, sender, "MasterTDCWindow")

# ! have yaml file flag
flag_yaml_exists = False

# * === Utility Functions ===
def append_file_extension(filename, extension=FILE_EXTENSION):
    return filename if filename.endswith(extension) else filename + extension

# * === Loading and Saving Configurations ===
def save_file_callback(sender, app_data):
    save_file_name = append_file_extension(app_data['file_path_name'])
    with open(save_file_name, 'w') as yaml_file:
        yaml.dump(register_map, yaml_file)
    print(f"Register map saved to {save_file_name}")

def load_file_callback(sender, app_data):
    load_file_name = append_file_extension(app_data['file_path_name'])
    with open(load_file_name, 'r') as yaml_file:
        global register_map
        register_map = yaml.load(yaml_file, Loader=yaml.FullLoader)

def update_register_map(group_id, reg_index, bit_index, value):
    global register_map
    print(f"Updating register map: group_id: {group_id}, reg_index: {reg_index}, bit_index: {bit_index}, value: {value}")
    register_map[group_id][reg_index] ^= (1 << bit_index)

# * === Window Update Function ===
def update_window():
    global register_map
    for key, values in register_map.items():
        text_id = f"text_{key}"
        text_value = ' '
        value_cnt = 0
        for value in values:
            value_cnt += 1
            text_value += f"{value:02X} "
            if value_cnt % 12 == 0:
                text_value += '\n '
        dpg.set_value(text_id, text_value)

def update_windows_size(main_width, main_height):
    global all_window_tags
    dpg.set_item_height("Primary Window", main_height)
    dpg.set_item_height("Register Map Monitor", main_height/3*2)
    dpg.set_item_pos("Register Map Monitor", pos=[main_width - dpg.get_item_width("Register Map Monitor"), 0])
    if Mode == "UDP":
        dpg.set_item_height("UDP Window", main_height/3)
        dpg.set_item_pos("UDP Window", pos=[main_width - dpg.get_item_width("UDP Window"), main_height/3*2])
    elif Mode == "Serial":
        dpg.set_item_height("Serial Window", main_height/3)
        dpg.set_item_pos("Serial Window", pos=[main_width - dpg.get_item_width("Serial Window"), main_height/3*2])

    for window_name in all_window_tags:
        if dpg.does_item_exist(window_name):
            dpg.set_item_height(window_name, main_height)
            dpg.set_item_width(window_name, main_width - dpg.get_item_width("Register Map Monitor") - dpg.get_item_width("Primary Window"))
            dpg.set_item_pos(window_name, pos=[dpg.get_item_width("Primary Window"), 0])
        
# * === Button Click Callback ===
def on_button_clicked(sender, app_data, user_data):
    print(f"Button {sender} clicked!")
    with dpg.window(label="new window"):
        dpg.add_text(f"{user_data}")

# * === Load Wizard Configuration ===
def load_reg_config():
    with open(REG_CONFIG_PATH) as reg_config_file:
        return json.load(reg_config_file)

def extract_default_byte_values(reg_config_data, reg_group='registers_top'):
    default_byte_values = []
    for register in reg_config_data[reg_group]:
        for reg_name, bits in register.items():
            _byte = sum((1 << bit['bit_number']) for bit in bits if bit['default_value'] == 1)
            default_byte_values.append(_byte)
    return default_byte_values

reg_config_data = load_reg_config()
default_byte_values_top = extract_default_byte_values(reg_config_data, 'registers_top')
default_byte_values_global_analog = extract_default_byte_values(reg_config_data, 'registers_global_analog')
default_byte_values_reference_voltage = extract_default_byte_values(reg_config_data, 'registers_reference_voltage')
default_byte_values_digital_half = extract_default_byte_values(reg_config_data, 'registers_digital_half')
default_byte_values_master_tdc = extract_default_byte_values(reg_config_data, 'registers_master_tdc')
default_byte_values_channel_wise = extract_default_byte_values(reg_config_data, 'registers_channel_wise')

# print("Default byte values of top sub-block:", default_byte_values_top)
# print("Default byte values of channel-wise:", default_byte_values_channel_wise)

# * main window size
main_window_width  = 1300
main_window_height = 850

i2c_address_names = [group for group in reg_config_data['I2C_address']]
i2c_address_vals  = [reg_config_data['I2C_address'][group] for group in i2c_address_names]
num_i2c_addresses = len(i2c_address_names)

global register_map
register_map = {reg_name: [] for reg_name in i2c_address_names}

i2c_channel_wise_names = []
i2c_channel_wise_vals  = []
i2c_reference_voltage_names = []
i2c_reference_voltage_vals  = []
i2c_global_analog_names = []
i2c_global_analog_vals  = []
i2c_digital_half_names = []
i2c_digital_half_vals  = []
i2c_master_tdc_names = []
i2c_master_tdc_vals  = []
i2c_top_sub_block_names = []
i2c_top_sub_block_vals  = []

# sort them according to their names

for i in range(num_i2c_addresses):
    _name = i2c_address_names[i]
    _val = i2c_address_vals[i]
    # if has 'Channel_' or 'CALIB_' or 'CM_', it is a channel-wise register
    if 'Channel_' in _name or 'CALIB_' in _name or 'CM_' in _name or 'HalfWise_' in _name:
        i2c_channel_wise_names.append(_name)
        i2c_channel_wise_vals.append(_val)
        for j in range(15):
            register_map[_name].append(default_byte_values_channel_wise[j])
    elif 'Reference_Voltage_' in _name:
        i2c_reference_voltage_names.append(_name)
        i2c_reference_voltage_vals.append(_val)
        for j in range(11):
            register_map[_name].append(default_byte_values_reference_voltage[j])
    elif 'Global_Analog_' in _name:
        i2c_global_analog_names.append(_name)
        i2c_global_analog_vals.append(_val)
        for j in range(15):
            register_map[_name].append(default_byte_values_global_analog[j])
    elif 'Digital_Half_' in _name:
        i2c_digital_half_names.append(_name)
        i2c_digital_half_vals.append(_val)
        for j in range(27):
            register_map[_name].append(default_byte_values_digital_half[j])
    elif 'Master_TDC_' in _name:
        i2c_master_tdc_names.append(_name)
        i2c_master_tdc_vals.append(_val)
        for j in range(16):
            register_map[_name].append(default_byte_values_master_tdc[j])
    elif 'Top' in _name:
        i2c_top_sub_block_names.append(_name)
        i2c_top_sub_block_vals.append(_val)
        for j in range(21):
            register_map[_name].append(default_byte_values_top[j])
    else:
        print(f"Unknown register name: {_name}")


def sort_and_briefify(names, values, replacements):
    name_val_dict = dict(zip(names, values))
    sorted_dict = dict(sorted(name_val_dict.items(), key=lambda item: item[0]))
    sorted_names = list(sorted_dict.keys())
    sorted_vals = list(sorted_dict.values())
    
    # Brief names
    brief_names = []
    for name in sorted_names:
        for orig, repl in replacements.items():
            name = name.replace(orig, repl)
        brief_names.append(name)
    return sorted_names, sorted_vals, brief_names  

i2c_channel_wise_names, i2c_channel_wise_vals, i2c_channel_wise_names_brief = sort_and_briefify(i2c_channel_wise_names, i2c_channel_wise_vals, {'Channel_': 'Ch', 'CALIB_': 'CAL', 'CM_': 'CM', 'HalfWise_': 'Half'})
i2c_reference_voltage_names, i2c_reference_voltage_vals, i2c_reference_voltage_names_brief = sort_and_briefify(i2c_reference_voltage_names, i2c_reference_voltage_vals, {'Reference_Voltage_': 'RefVolt_'})
i2c_global_analog_names, i2c_global_analog_vals, _ = sort_and_briefify(i2c_global_analog_names, i2c_global_analog_vals, {})
i2c_digital_half_names, i2c_digital_half_vals, _ = sort_and_briefify(i2c_digital_half_names, i2c_digital_half_vals, {})
i2c_master_tdc_names, i2c_master_tdc_vals, _ = sort_and_briefify(i2c_master_tdc_names, i2c_master_tdc_vals, {})
i2c_top_sub_block_names, i2c_top_sub_block_vals, _ = sort_and_briefify(i2c_top_sub_block_names, i2c_top_sub_block_vals, {})

# !! Create a window for each register group
dpg.create_context()

with dpg.window(tag="Register Map Monitor", width=RIGHT_SIDE_WINDOW_WIDTH, height=main_window_height, no_title_bar=False):
    dpg.set_item_pos("Register Map Monitor", pos=[main_window_width - RIGHT_SIDE_WINDOW_WIDTH, 0])
    dpg.set_item_label("Register Map Monitor", "Register Map")
    for key, values in register_map.items():
        text_id = f"text_{key}"
        text_value = ''
        for value in values:
            text_value += f"{value:02X} "
        dpg.add_text(f'{key}: ')
        dpg.add_text(text_value, tag=text_id)

# * === Loading and Saving Configurations ===
with dpg.file_dialog(
    directory_selector=False, show=False, callback=save_file_callback, tag="file_dialog_save", width=600 ,height=400):
    dpg.add_file_extension(".yaml", color=(255, 255, 0, 255), custom_text="YAML")

with dpg.file_dialog(
    directory_selector=False, show=False, callback=load_file_callback, tag="file_dialog_load", width=600 ,height=400):
    dpg.add_file_extension(".yaml", color=(255, 255, 0, 255), custom_text="YAML")
# * === End of Loading and Saving Configurations ===

with dpg.font_registry():
    default_font = dpg.add_font("assets/fonts/UbuntuMono-Regular.ttf", 14)

with dpg.window(tag="Full Window", width=main_window_width, height=main_window_height, no_title_bar=True):
    pass

with dpg.window(tag="Serial Window", width=RIGHT_SIDE_WINDOW_WIDTH, height=main_window_height/3, no_title_bar=False, show=(Mode=="Serial")):
    dpg.set_item_pos("Serial Window", pos=[main_window_width - RIGHT_SIDE_WINDOW_WIDTH, main_window_height/3*2])
    dpg.set_item_label("Serial Window", "Serial Link")
    dpg.add_text("Serial Port")
    dpg.add_input_text(label="##serial_port", width=200, default_value=SerialPort, callback=on_serial_port_changed, tag="input_serial_port")
    dpg.add_text("Baud Rate")
    dpg.add_input_text(label="##baud_rate", width=200, default_value=BaudRate, callback=on_baud_rate_changed, tag="input_baud_rate")
    dpg.add_text("H2GCROC ID")
    dpg.add_input_text(label="##h2gcroc_id", width=200, default_value=H2GCROC_ID, callback=on_h2gcroc_id_changed, tag="input_h2gcroc_id")

    dpg.add_spacer(height=20)
    dpg.add_text("Send Commands")
    with dpg.group(horizontal=True):
        dpg.add_button(label="Send One", width=128, height=20, callback=on_serial_send_one_clicked)
        dpg.add_button(label="Send All", width=128, height=20, callback=on_serial_send_all_clicked)

with dpg.window(tag="UDP Window", width=RIGHT_SIDE_WINDOW_WIDTH, height=main_window_height/3, no_title_bar=False, show=(Mode=="UDP")):
    dpg.set_item_pos("UDP Window", pos=[main_window_width - RIGHT_SIDE_WINDOW_WIDTH, 0])
    dpg.set_item_label("UDP Window", "UDP Link")
    dpg.add_text("UDP Target IP")
    dpg.add_input_text(label="##udp_ip", width=200, default_value=UDP_IP_TARGET, callback=on_udp_ip_target_changed, tag="input_udp_ip")
    dpg.add_text("UDP Target Port")
    dpg.add_input_text(label="##udp_port", width=200, default_value=UDP_PORT_TARGET, callback=on_udp_port_target_changed, tag="input_udp_port")
    dpg.add_text("UDP Source IP")
    dpg.add_input_text(label="##udp_source_ip", width=200, default_value=UDP_IP_SOURCE, callback=on_udp_ip_source_changed, tag="input_udp_source_ip")
    dpg.add_text("UDP Source Port")
    dpg.add_input_text(label="##udp_source_port", width=200, default_value=UDP_PORT_SOURCE, callback=on_udp_port_source_changed, tag="input_udp_source_port")

    dpg.add_text("Send Commands")
    with dpg.group(horizontal=True):
        dpg.add_button(label="Send One", width=128, height=20, callback=on_udp_send_one_clicked)
        dpg.add_button(label="Send All", width=128, height=20, callback=on_udp_send_all_clicked)

with dpg.window(tag="Primary Window", width=LEFT_SIDE_WINDOW_WIDTH, height=main_window_height, no_title_bar=True):
    # set position of the window
    dpg.set_item_pos("Primary Window", pos=[0, 0])
    dpg.bind_font(default_font)
    # create buttons for each I2C address into a group
    dpg.add_text("Top sub-block registers")
    with dpg.group(horizontal=True):
        for i in range(len(i2c_top_sub_block_names)):
            dpg.add_button(label=i2c_top_sub_block_names[i], width=264, height=20, tag=i2c_top_sub_block_names[i], callback=on_top_sub_module_clicked, user_data=register_map[i2c_top_sub_block_names[i]])

    dpg.add_text("Channel registers")

    button_width = 60
    button_height = 20

    buttons_per_row = 4
    num_rows = (len(i2c_channel_wise_names) + buttons_per_row - 1) // buttons_per_row

    for row in range(num_rows):
        with dpg.group(horizontal=True):  # Create a horizontal group for each row
            for col in range(buttons_per_row):
                idx = row * buttons_per_row + col
                if idx < len(i2c_channel_wise_names):  # Make sure we don't exceed the list length
                    dpg.add_button(label=i2c_channel_wise_names_brief[idx], width=button_width, height=button_height, callback=on_channel_module_clicked, user_data=register_map[i2c_channel_wise_names[idx]], tag=i2c_channel_wise_names[idx])

    dpg.add_text("Reference voltage registers")
    with dpg.group(horizontal=True):
        for i in range(len(i2c_reference_voltage_names)):
            dpg.add_button(label=i2c_reference_voltage_names_brief[i], width=128, height=20, tag=i2c_reference_voltage_names[i], callback=on_reference_voltage_module_clicked, user_data=register_map[i2c_reference_voltage_names[i]])
    
    dpg.add_text("Global analog registers")
    with dpg.group(horizontal=True):
        for i in range(len(i2c_global_analog_names)):
            dpg.add_button(label=i2c_global_analog_names[i], width=128, height=20, callback=on_global_analog_module_clicked, user_data=register_map[i2c_global_analog_names[i]],tag=i2c_global_analog_names[i])

    dpg.add_text("Digital half registers")
    with dpg.group(horizontal=True):
        for i in range(len(i2c_digital_half_names)):
            dpg.add_button(label=i2c_digital_half_names[i], width=128, height=20, tag=i2c_digital_half_names[i], callback=on_digital_half_module_clicked, user_data=register_map[i2c_digital_half_names[i]])

    dpg.add_text("Master TDC registers")
    with dpg.group(horizontal=True):
        for i in range(len(i2c_master_tdc_names)):
            dpg.add_button(label=i2c_master_tdc_names[i], width=128, height=20, tag=i2c_master_tdc_names[i], callback=on_master_tdc_module_clicked, user_data=register_map[i2c_master_tdc_names[i]])

    # add space
    dpg.add_spacer(height=20)

    dpg.add_text("Save/Load Configuration")
    with dpg.group(horizontal=True):
        dpg.add_button(label="Save", width=128, height=20, callback=lambda: dpg.show_item("file_dialog_save"))
        dpg.add_button(label="Load", width=128, height=20, callback=lambda: dpg.show_item("file_dialog_load"))

dpg.create_viewport(title='H2GWizard', width=main_window_width, height=main_window_height)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.set_primary_window("Full Window", True)

main_loop_cnt = 0

while dpg.is_dearpygui_running():
    main_loop_cnt += 1
    # set value of each register
    update_window()
    # get main window size
    if main_loop_cnt >= 10:
        main_window_width, main_window_height = dpg.get_item_rect_size("Full Window")
        # print("main window width: ", main_window_width)
        # print("main window height: ", main_window_height)
        update_windows_size(main_window_width, main_window_height)
    for name in i2c_top_sub_block_names:
        dpg.configure_item(name, user_data=register_map[name])
    dpg.render_dearpygui_frame()

on_close_callback(None, None)
dpg.destroy_context()