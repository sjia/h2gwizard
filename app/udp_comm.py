import socket
import json

is_connected = False
REG_CONFIG_PATH = 'assets/reg_config/h2gcroc_1v4_r1.json'

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def load_reg_config():
    with open(REG_CONFIG_PATH) as reg_config_file:
        return json.load(reg_config_file)
    
def create_udp_connection(ip_target, port_target, ip_local, port_local):
    global udp_socket
    if str(type(port_local)) == "<class 'str'>":
        port_local = int(port_local)
    udp_socket.bind((ip_local, port_local))
    # check if ip is available
    if ip_target is None:
        raise Exception("IP is not specified")
    # check if port is available
    if port_target is None:
        raise Exception("Port is not specified")
    # check if ip local is available
    if ip_local is None:
        raise Exception("Local IP is not specified")
    # check if port local is available
    if port_local is None:
        raise Exception("Local Port is not specified")
    # create udp connection
    udp_socket.connect((ip_target, port_target))
    print("Connected to " + ip_target + ":" + str(port_target))

def send_one_group_to_h2gcroc(ip_target, port_target, ip_local, port_local, h2gcrocid, keyname, valuelist):
    global udp_socket
    global is_connected
    if is_connected == False:
        try:
            create_udp_connection(ip_target, port_target, ip_local, port_local)
            is_connected = True
        except Exception as e:
            print(e)
            is_connected = False
            return
    else:
        current_ip, current_port = udp_socket.getsockname()
        if current_ip != ip_local or str(current_port) != port_local:
            try:
                print("Mismatch in local IP and port. Reconnecting...")
                udp_socket.close()
                udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                create_udp_connection(ip_target, port_target, ip_local, port_local)
                is_connected = True
            except Exception as e:
                print(e)
                is_connected = False
                return
    
    print("keyname: " + keyname)
    reg_config_data = load_reg_config()
    try:
        i2c_address = reg_config_data["I2C_address"][keyname]
    except Exception as e:
        print(e)
        return
    
    list_to_send = []

    for value in valuelist:

        list_to_send.append(value)
    cmd_len = len(list_to_send)

    list_to_send.insert(0, (i2c_address & 0x07) << 5)
    list_to_send.insert(0, (i2c_address >> 3))
    list_to_send.insert(0, cmd_len)

    list_to_send.insert(0, 0x00)
    list_to_send.insert(0, 0x00)
    list_to_send.insert(0, 0x00)

    list_to_send.insert(0, 0x03)
    list_to_send.insert(0, int(h2gcrocid, 16))

    while len(list_to_send) < 40:
        list_to_send.append(0x00)

    try:
        udp_socket.send(bytes(list_to_send))
        print("Sent to " + ip_target + ":" + str(port_target))
    except Exception as e:
        print(e)
        is_connected = False
        return