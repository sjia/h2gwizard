# H2GWizard

[![Made with DearPyGui](https://img.shields.io/badge/Made%20with-DearPyGui-blue.svg)](https://github.com/hoffstadt/DearPyGui) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Dependencies

- [DearPyGui](https://github.com/hoffstadt/DearPyGui?tab=readme-ov-file#installation)
    You could install DearPyGui by:
    ``` shell
    pip install dearpygui
    ```
    or
    ``` shell
    pip3 install dearpygui
    ```
- [YAML](https://pypi.org/project/PyYAML/)
    ``` shell
    pip install pyyaml
    ```
- [Serial](https://pypi.org/project/pyserial/)
    ``` shell
    pip install pyserial
    ```


## Usage

```bash
python3 app/config_manager.py
```

!!! Note
    The application is tested on Linux and MacOS with conda environment. It should work on Windows as well.

## Project Structure

```bash
.
├── LICENSE
├── README.md
├── doc
├── app
│   ├── serial_comm.py
│   ├── udp_comm.py
│   └── config_manager.py
└── assets
    ├── comm_config.json
    ├── fonts
    │   ├── UbuntuMono-Bold.ttf
    │   ├── UbuntuMono-BoldItalic.ttf
    │   ├── UbuntuMono-Italic.ttf
    │   └── UbuntuMono-Regular.ttf
    └── reg_config
        ├── h2gcroc_1v4_fastcmd.json
        └── h2gcroc_1v4_r1.json
```

The definition of the register groups and registers is stored in the `.json` files in the `assets/reg_config` folder. The default configuration is loaded from `assets/reg_config/h2gcroc_1v4_r1.json`.

## Main Window

![Main Window](doc/Mainwindow_Screenshot.png)

The main window is divided into 3 sections:

- **Left**: List of all available groups
- **Center**: List of all available registers of the selected register group
- **Right**: Register value browser

### Load and Save Configurations

The configuration can be loaded and saved using the buttons in the bottom left corner.

![Load and Save Configurations](doc/Svae&Load_Buttons.png){width="400px"}

By opening the wizard, the default configuration is loaded from values in `assets/reg_config/h2gcroc_1v4_r1.json`. The configuration can be saved to a `.yaml` file using the `Save` button. The `Load` button opens a file dialog to select a saved configuration file.

### Serial Communication

On the bottom right corner, the serial communication can be configured.
- **Port**: Select the serial port. For Linux and MacOS, the port is usually `/dev/ttyUSB0` or `/dev/ttyUSB1`. For Windows, the port is usually `COM1` or `COM2`.
- **Baudrate**: Select the baudrate. The default baudrate is `115200`.
- **H2GCROC ID**: Select the H2GCROC to configure. The default ID is `0xA5`.

By clicking `Send One` button, the register group shown in the center will be sent to the H2GCROC. By clicking `Send All` button, all register groups will be sent.

### UDP Communication

To change the default communication method from serial to UDP, you can change the value of `Mode` in the `config_manager.py` file `line 31`.

- **UDP IP**: Enter the IP address of the H2GCROC.
- **UDP Port**: Enter the port number of the H2GCROC.

By clicking `Send One` button, the register group shown in the center will be sent to the H2GCROC. By clicking `Send All` button, all register groups will be sent.
